
# Python stdlib imports
from StringIO import StringIO  # cStringIO doesn't handle unicode

# package imports
from openpyxl.shared.xmltools import XMLGenerator, get_document_content, start_tag, end_tag, tag

def write_tablepart(tablepart, id):
    xml_file = StringIO()
    doc = XMLGenerator(xml_file, 'utf-8')
    start_tag(doc, 'table',
            {'xmlns': 'http://schemas.openxmlformats.org/spreadsheetml/2006/main',
             'id': id,
             'name': tablepart.name,
             'displayName': tablepart.displayName,
             'ref': tablepart.ref,
             'totalsRowShown': tablepart.totalsRowShown
             })
    tag(doc, 'autoFilter', {'ref': tablepart.ref})
    start_tag(doc, 'tableColumns', {'count': str(len(tablepart.columns))})
    for i, column_name in enumerate(tablepart.columns):
        tag(doc, 'tableColumn', {'id': str(i+1),'name': column_name,})
    end_tag(doc, 'tableColumns')
    tag(doc, 'tableStyleInfo', {
        'name':'TableStyleMedium2',
        'showFirstColumn':tablepart.showFirstColumn, 'showLastColumn':tablepart.showLastColumn,
        'showRowStripes':tablepart.showRowStripes, 'showColumnStripes':tablepart.showColumnStripes })
    end_tag(doc, 'table')
    doc.endDocument()
    xml_string = xml_file.getvalue()
    xml_file.close()
    return xml_string
    
