
class Tablepart(object):
    """Represents a tablepart.

    Do not create tableparts yourself,
    use :func:`openpyxl.workbook.Worksheet.add_tablepart` instead

    """
    def __init__(self, name, displayName, ref, totalsRowShown,
                 columns, showFirstColumn, showLastColumn, showRowStripes, showColumnStripes):
        self.name=name
        self.displayName = displayName
        self.ref = ref
        self.totalsRowShown = totalsRowShown
        self.columns = columns
        self.showFirstColumn = showFirstColumn
        self.showLastColumn = showLastColumn
        self.showRowStripes = showRowStripes
        self.showColumnStripes = showColumnStripes
        
